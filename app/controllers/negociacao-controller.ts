import { DiasDaSemana } from "../enums/dias-da-semana.js";
import { Negociacao } from "../models/negociacao.js";
import { Negociacoes } from "../models/negociacoes.js";
import { MensagemView } from "../views/mensagem-view.js";
import { NegociacoesView } from "../views/negociacoes-view.js";

export class NegociacaoController{

    private inputData:HTMLInputElement;
    private inputQuantidade:HTMLInputElement;
    private inputValor:HTMLInputElement;
    private negociacoes:Negociacoes= new Negociacoes();
    private negociacoesView = new NegociacoesView('#negociacoesView');
    private mensagemView= new MensagemView('#mensagemView');
    private readonly SABADO=6;
    private readonly DOMINGO=0;

    constructor (){
        this.inputData=document.querySelector('#data');
        this.inputQuantidade=document.querySelector('#quantidade');
        this.inputValor=document.querySelector('#valor');
        //this.negociacoesView.template();
        this.negociacoesView.update(this.negociacoes);
    }
    adiciona(): void{
        const negociacao = Negociacao.criaDe(this.inputData.value,this.inputQuantidade.value,this.inputValor.value);
        if (!this.isDiaUtil(negociacao.data)){
            this.mensagemView.update('Apenas dias úteis são aceitos');
            return;
        }
        
        this.negociacoes.adiciona(negociacao);
        this.negociacoesView.update(this.negociacoes);
        this.mensagemView.update('Negociação adicionada com sucesso');
        this.limparFormulario();
        
    }

    private isDiaUtil(date:Date){
        return (date.getDay() > DiasDaSemana.DOMINGO && date.getDay() < DiasDaSemana.SABADO);

    }
    

    limparFormulario():void{
        this.inputData.value='';
        this.inputQuantidade.value='';
        this.inputValor.value='';
        this.inputData.focus();
    }

    
    
}