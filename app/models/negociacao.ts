export class Negociacao {
    

    constructor(public readonly data: Date,public readonly quantidade:number,public readonly valor: number){
       
    }

    

    get volume(){
        return this.quantidade * this.valor;
    }

    public static criaDe(dataString:string, quantidadeString: string, valorString:string){
        const exp = /-/g;
        const date= new Date(dataString.replace(exp,','));
        const quantidade= parseInt(quantidadeString);
        const valor = parseFloat(valorString);
        const negociacao = new Negociacao(
            
            date,
            quantidade,
            valor);
            return negociacao;
    }
}